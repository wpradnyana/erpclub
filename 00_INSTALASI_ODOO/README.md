# Catatan instalasi Odoo 12 pada Windows

## Instalasi software keperluan

### Visual Studio Code
Download dari https://aka.ms/win32-x64-user-stable

### DBeaver CE
Download dari https://dbeaver.io/files/dbeaver-ce-latest-win32.win32.x86_64.zip 

### Postgresql 10 
Download dari https://sbp.enterprisedb.com/getfile.jsp?fileid=12544 .
Saat instalasi, gunakan password yang mudah diingat untuk login ke database sebagai user 'postgres' yang memiliki role DBA, misalkan 'Password.123'

### Python 3.7.8
Download dari https://www.python.org/ftp/python/3.7.8/python-3.7.8-amd64.exe

Beberapa catatan :
- Python 3.8 belum bagus dalam dukungan library-library yg digunakan Odoo 12


### Odoo 12 Source code
Download dari https://nightly.odoo.com/12.0/nightly/src/odoo_12.0.latest.zip, simpan ke folder D:/odoo/ .
Ekstrak ke folder tersebut dan atur penamaan dan letak folder sehingga terdapat file **setup.py** pada **D:\odoo\odoo12\odoo-12.0** . 
Buat juga sebuah direktori **D:\odoo\erpclub** untuk proyek modul kustom.

Copy file odoo-bin dari folder setup ke folder D:\odoo\odoo12\odoo-12.0 .

Buat sebuah file **odoo-mini.conf** pada folder tersebut berisikan :
````
[options]
addons_path = D:\odoo\erpclub,D:\odoo\odoo12\odoo-12.0\odoo\addons
data_dir = ..\data
db_host = localhost
db_name = odoo12base
db_password = odoo
db_port = 5432
db_sslmode = prefer
db_template = template0
db_user = odoo
dbfilter = .*
demo = {}
dev = all
list_db = True
server_wide_modules = base,web
translate_modules = ['all']
install = []
update = []
````

### Membuat koneksi administrasi PostgreSQL

Jalankan aplikasi **DBeaver** untuk melakukan manajemen database.
Buat sebuah koneksi ke database postgresql lokal , *File -> DBeaver-> Database Connection* , pilih **PostgreSQL**, gunakan data berikut.
```
host=localhost
database=postgres
user=postgres
password=Password.123
```
Pada tab PostgreSQL pilih opsi **Show all databases**.
Setelah berhasil koneksi dengan konfigurasi tersebut, pada tree **postgres** klik kanan *Create->Role* buat sebuah role atau akun login dengan nama **odoo** dan password **odoo** , pilih juga opsi **Super User** dan **Can Login**. Finish dan refresh tree **postgres**.
Klik kanan juga pada **postgres**  *Create->Database* buat database **odoo12base** dengan owner **odoo**. Finish dan refresh.

Daftarkan koneksi Odoo12base tersebut dengan user odoo/odoo pada DBeaver dengan *File -> DBeaver -> Database Connection* seperti pada tahap sebelumnya, namun tanpa **Show all databases**. 

### Instalasi library Python pendukung Odoo 12
Buka window **Command Prompt** pada direktori **D:\odoo\odoo12\odoo-12.0**.
Ketik perintah berikut pada direktori tersebut.
```
pip3 install -r requirements.txt
```
Tunggu hingga selesai dan komplit. 

### Menjalankan konfigurasi awal 
Jalankan perintah berikut untuk menginisialisasi tabel dan data awal untuk Odoo.
```
python odoo-bin -c odoo-mini.conf -i base --stop-after-init
```
Setelah proses inisialisasi database maka akan dikembalikan ke command prompt untuk memulai Odoo

### Startup Odoo
Jalankan perintah baris berikut untuk memulai aplikasi Odoo.
```
python odoo-bin -c odoo-mini.conf
```
Untuk menghentikan Odoo, tinggal menekan tombol **Ctrl+C** pada command prompt.
Jika memerlukan informasi debug saat menjalankan Odoo, lakukan perintah baris berikut.

```
python odoo-bin -c odoo-mini.conf
```

### Mengkonfigurasikan IDE untuk Odoo 
Jalankan aplikasi Visual Studio Code.
Pada manajemen extension, pilih **Odoo Snippets** dan **Arkademy - Odoo** sebagai plugin pendukung pengembangan modul Odoo.
Import folder untuk proyek dengan *File->Open Folder* arahkan ke direktori **D:\odoo\erpclub**

# Selamat Bekerja 




