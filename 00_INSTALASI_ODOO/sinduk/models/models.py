# -*- coding: utf-8 -*-

from odoo import models, fields, api, tools


# class catatpenduduk(models.Model):
#     _name = 'catatpenduduk.catatpenduduk'
#     _description = 'catatpenduduk.catatpenduduk'

#     name = fields.Char()
#     value = fields.Integer()
#     value2 = fields.Float(compute="_value_pc", store=True)
#     description = fields.Text()
#
#     @api.depends('value')
#     def _value_pc(self):
#         for record in self:
#             record.value2 = float(record.value) / 100


class SindukMsPetugas(models.Model):
    _name = 'sinduk.ms.petugas'
    _description = 'Sinduk Master Petugas'
    # _inherit = 'hr.employee'
    _rec_name = 'name'
    _order = 'name ASC'

    name = fields.Char(
        string='Name',
        required=True,
        # default=lambda self: _('New'),
        copy=False
    )

    kode_petugas = fields.Text(
        string='Kode Petugas',
    )

    employee_id = fields.Many2one(
        string='Karyawan',
        comodel_name='hr.employee',
        ondelete='restrict',
    )

    # category_ids = fields.Many2many(
    #     string='category',
    #     comodel_name='hr.employee.category',
    #     relation='hr_employee_category_petugas_rel',
    #     column1='hr_employee_category_id',
    #     column2='petugas_id',
    # )


class SindukRefCodeGroup(models.Model):
    _name = 'sinduk.ref.code.group'
    _description = 'Sinduk Ref Code Group'

    _rec_name = 'name'
    _order = 'name ASC'

    name = fields.Char(
        string='Name',
        required=True,
        # default=lambda self: _('New'),
        copy=False
    )

    group = fields.Text(
        string='Group',
    )

    deskripsi = fields.Text(
        string='Deskripsi',
    )


class SindukRefCodes(models.Model):
    _name = 'sinduk.ref.codes'
    _description = 'Sinduk Ref Code'
    _rec_name = 'name'
    _order = 'name ASC'
    _table = 'sinduk_ref_codes'

    name = fields.Char(
        string='Name',
        required=True,
        # default=lambda self: _('New'),
        copy=False
    )

    code = fields.Text(
        string='Kode',
    )

    deskripsi = fields.Text(
        string='Deskripsi',
    )

    aktif = fields.Boolean(
        string='Aktif',
    )

    @api.model
    def _get_default_group(self):
        res = self.env['sinduk.ref.code.group'].search(
            [('name', '=', 'Agama')])
        return res.id or False

    code_group_id = fields.Many2one(
        string='Code Group',
        comodel_name='sinduk.ref.code.group',
        ondelete='restrict',
        default=_get_default_group
    )


# class SindukRefCodeJenisKelamin(models.Model):
#     _name = 'sinduk.ref.code.jenis.kelamin'
#     _inherit = 'sinduk.ref.codes'
#     _rec_name = 'name'
#     _order = 'name ASC'
#     _description = 'Jenis Kelamin'
#     _table = 'sinduk_ref_codes'

#     name = fields.Char(
#         string='Name',
#         required=True,
#         # default=lambda self: _('New'),
#         copy=False
#     )

class SindukRefCodeJenisKelamin(models.Model):
    _name = 'sinduk.ref.code.jenis.kelamin'
    _inherit = 'sinduk.ref.codes'
    _auto = False

    name = fields.Char(
        string='Name',
        required=True,
        # default=lambda self: _('New'),
        copy=False
    )
    code = fields.Text(
        string='Kode', readonly=True
    )

    deskripsi = fields.Text(
        string='Deskripsi', readonly=True
    )

    aktif = fields.Boolean(
        string='Aktif', readonly=True
    )

    code_group_id = fields.Many2one(
        string='Code Group',
        comodel_name='sinduk.ref.code.group',
        ondelete='restrict', readonly=True
    )

    def init(self):
        tools.drop_view_if_exists(self._cr, 'sinduk_ref_code_jenis_kelamin')
        self._cr.execute(""" CREATE VIEW sinduk_ref_code_jenis_kelamin AS (
            select 
            src.* 
            from sinduk_ref_codes src
            left join sinduk_ref_code_group srcgr
            on
            src.code_group_id=srcgr.id
            where srcgr.group='Jenis Kelamin'
            )""")


# class SindukRefCodeGolonganDarah(models.Model):
#     _name = 'sinduk.ref.code.golongan.darah'
#     _inherit = 'sinduk.ref.codes'
#     _rec_name = 'name'
#     _description = 'Golongan Darah'
#     _order = 'name ASC'
#     _table = 'sinduk_ref_codes'

#     name = fields.Char(
#         string='Name',
#         required=True,
#         # default=lambda self: _('New'),
#         copy=False
#     )

class SindukRefCodeGolonganDarah(models.Model):
    _name = 'sinduk.ref.code.golongan.darah'
    _auto = False

    name = fields.Char(
        string='Name',
        required=True,
        # default=lambda self: _('New'),
        copy=False
    )
    code = fields.Text(
        string='Kode', readonly=True
    )

    deskripsi = fields.Text(
        string='Deskripsi', readonly=True
    )

    aktif = fields.Boolean(
        string='Aktif', readonly=True
    )

    code_group_id = fields.Many2one(
        string='Code Group',
        comodel_name='sinduk.ref.code.group',
        ondelete='restrict', readonly=True
    )

    def init(self):
        tools.drop_view_if_exists(self._cr, 'sinduk_ref_code_golongan_darah')
        self._cr.execute(""" CREATE VIEW sinduk_ref_code_golongan_darah AS (
            select 
            src.* 
            from sinduk_ref_codes src
            left join sinduk_ref_code_group srcgr
            on
            src.code_group_id=srcgr.id
            where srcgr.group='Golongan Darah'
            )""")


# class SindukRefCodeAgama(models.Model):
#     _name = 'sinduk.ref.code.agama'
#     _inherit = 'sinduk.ref.codes'
#     _rec_name = 'name'
#     _description = 'Agama'
#     _order = 'name ASC'
#     _table = 'sinduk_ref_codes'


#     name = fields.Char(
#         string='Name',
#         required=True,
#         # default=lambda self: _('New'),
#         copy=False
#     )
#     code_group_id = fields.Many2one(
#         string='Code Group',
#         comodel_name='sinduk.ref.code.group',
#         ondelete='restrict',
#         default=3
#     )

class SindukRefCodeAgama(models.Model):
    _name = 'sinduk.ref.code.agama'
    _auto = False
    name = fields.Char(
        string='Name',
        required=True,
        # default=lambda self: _('New'),
        copy=False
    )

    code = fields.Text(
        string='Kode', readonly=True
    )

    deskripsi = fields.Text(
        string='Deskripsi', readonly=True
    )

    aktif = fields.Boolean(
        string='Aktif', readonly=True
    )

    code_group_id = fields.Many2one(
        string='Code Group',
        comodel_name='sinduk.ref.code.group',
        ondelete='restrict', readonly=True
    )

    def init(self):
        tools.drop_view_if_exists(self._cr, 'sinduk_ref_code_agama')
        self._cr.execute(""" CREATE VIEW sinduk_ref_code_agama AS (
            select 
            src.* 
            from sinduk_ref_codes src
            left join sinduk_ref_code_group srcgr
            on
            src.code_group_id=srcgr.id
            where srcgr.group='Agama'
            )""")


# class SindukRefCodePendidikanTerakhir(models.Model):
#     _name = 'sinduk.ref.code.pendidikan.terakhir'
#     _inherit = 'sinduk.ref.codes'
#     _rec_name = 'name'
#     _description = 'Pendidikan Terakhir'
#     _order = 'name ASC'
#     _table = 'sinduk_ref_codes'

#     name = fields.Char(
#         string='Name',
#         required=True,
#         # default=lambda self: _('New'),
#         copy=False
#     )

class SindukRefCodePendidikanTerakhir(models.Model):
    _name = 'sinduk.ref.code.pendidikan.terakhir'
    _auto = False
    name = fields.Char(
        string='Name',
        required=True,
        # default=lambda self: _('New'),
        copy=False
    )

    code = fields.Text(
        string='Kode', readonly=True
    )

    deskripsi = fields.Text(
        string='Deskripsi', readonly=True
    )

    aktif = fields.Boolean(
        string='Aktif', readonly=True
    )

    code_group_id = fields.Many2one(
        string='Code Group',
        comodel_name='sinduk.ref.code.group',
        ondelete='restrict', readonly=True
    )

    def init(self):
        tools.drop_view_if_exists(
            self._cr, 'sinduk_ref_code_pendidikan_terakhir')
        self._cr.execute(""" CREATE VIEW sinduk_ref_code_pendidikan_terakhir AS (
            select 
            src.* 
            from sinduk_ref_codes src
            left join sinduk_ref_code_group srcgr
            on
            src.code_group_id=srcgr.id
            where srcgr.group='Pendidikan Terakhir'
            )""")

# class SindukRefCodeProvinsi(models.Model):
#     _name = 'sinduk.ref.code.provinsi'
#     _inherit = 'sinduk.ref.codes'
#     _rec_name = 'name'
#     _description = 'Provinsi'
#     _order = 'name ASC'
#     _table = 'sinduk_ref_codes'

#     name = fields.Char(
#         string='Name',
#         required=True,
#         # default=lambda self: _('New'),
#         copy=False
#     )


class SindukRefCodeProvinsi(models.Model):
    _name = 'sinduk.ref.code.provinsi'
    _auto = False
    name = fields.Char(
        string='Name',
        required=True,
        # default=lambda self: _('New'),
        copy=False
    )

    code = fields.Text(
        string='Kode', readonly=True
    )

    deskripsi = fields.Text(
        string='Deskripsi', readonly=True
    )

    aktif = fields.Boolean(
        string='Aktif', readonly=True
    )

    code_group_id = fields.Many2one(
        string='Code Group',
        comodel_name='sinduk.ref.code.group',
        ondelete='restrict', readonly=True
    )

    def init(self):
        tools.drop_view_if_exists(self._cr, 'sinduk_ref_code_provinsi')
        self._cr.execute(""" CREATE VIEW sinduk_ref_code_provinsi AS (
            select 
            src.* 
            from sinduk_ref_codes src
            left join sinduk_ref_code_group srcgr
            on
            src.code_group_id=srcgr.id
            where srcgr.group='Provinsi'
            )""")

# class SindukRefCodeStatusPenduduk(models.Model):
#     _name = 'sinduk.ref.code.status.penduduk'
#     _inherit = 'sinduk.ref.codes'
#     _rec_name = 'name'
#     _description = 'Status Penduduk'
#     _order = 'name ASC'
#     _table = 'sinduk_ref_codes'

#     name = fields.Char(
#         string='Name',
#         required=True,
#         # default=lambda self: _('New'),
#         copy=False
#     )


class SindukRefCodeStatusPenduduk(models.Model):
    _name = 'sinduk.ref.code.status.penduduk'
    _auto = False
    name = fields.Char(
        string='Name',
        required=True,
        # default=lambda self: _('New'),
        copy=False
    )

    code = fields.Text(
        string='Kode', readonly=True
    )

    deskripsi = fields.Text(
        string='Deskripsi', readonly=True
    )

    aktif = fields.Boolean(
        string='Aktif', readonly=True
    )

    code_group_id = fields.Many2one(
        string='Code Group',
        comodel_name='sinduk.ref.code.group',
        ondelete='restrict', readonly=True
    )

    def init(self):
        tools.drop_view_if_exists(self._cr, 'sinduk_ref_code_status_penduduk')
        self._cr.execute(""" CREATE VIEW sinduk_ref_code_status_penduduk AS (
            select 
            src.* 
            from sinduk_ref_codes src
            left join sinduk_ref_code_group srcgr
            on
            src.code_group_id=srcgr.id
            where srcgr.group='Status Penduduk'
            )""")

# class SindukRefCodeStatusKawin(models.Model):
#     _name = 'sinduk.ref.code.status.kawin'
#     _inherit = 'sinduk.ref.codes'
#     _rec_name = 'name'
#     _description = 'Status Kawin'
#     _order = 'name ASC'
#     _table = 'sinduk_ref_codes'

#     name = fields.Char(
#         string='Name',
#         required=True,
#         # default=lambda self: _('New'),
#         copy=False
#     )


class SindukRefCodeStatusKawin(models.Model):
    _name = 'sinduk.ref.code.status.kawin'
    _auto = False
    name = fields.Char(
        string='Name',
        required=True,
        # default=lambda self: _('New'),
        copy=False
    )

    code = fields.Text(
        string='Kode', readonly=True
    )

    deskripsi = fields.Text(
        string='Deskripsi', readonly=True
    )

    aktif = fields.Boolean(
        string='Aktif', readonly=True
    )

    code_group_id = fields.Many2one(
        string='Code Group',
        comodel_name='sinduk.ref.code.group',
        ondelete='restrict', readonly=True
    )

    def init(self):
        tools.drop_view_if_exists(self._cr, 'sinduk_ref_code_status_kawin')
        self._cr.execute(""" CREATE VIEW sinduk_ref_code_status_kawin AS (
            select 
            src.* 
            from sinduk_ref_codes src
            left join sinduk_ref_code_group srcgr
            on
            src.code_group_id=srcgr.id
            where srcgr.group='Status Kawin'
            )""")

# class SindukRefCodeKewarganegaraan(models.Model):
#     _name = 'sinduk.ref.code.kewarganegaraan'
#     _inherit = 'sinduk.ref.codes'
#     _rec_name = 'name'
#     _description = 'Kewarganegaraan'
#     _order = 'name ASC'
#     _table = 'sinduk_ref_codes'

#     name = fields.Char(
#         string='Name',
#         required=True,
#         # default=lambda self: _('New'),
#         copy=False
#     )


class SindukRefCodeKewarganegaraan(models.Model):
    _name = 'sinduk.ref.code.kewarganegaraan'
    _auto = False
    name = fields.Char(
        string='Name',
        required=True,
        # default=lambda self: _('New'),
        copy=False
    )

    code = fields.Text(
        string='Kode', readonly=True
    )

    deskripsi = fields.Text(
        string='Deskripsi', readonly=True
    )

    aktif = fields.Boolean(
        string='Aktif', readonly=True
    )

    code_group_id = fields.Many2one(
        string='Code Group',
        comodel_name='sinduk.ref.code.group',
        ondelete='restrict', readonly=True
    )

    def init(self):
        tools.drop_view_if_exists(self._cr, 'sinduk_ref_code_kewarganegaraan')
        self._cr.execute(""" CREATE VIEW sinduk_ref_code_kewarganegaraan AS (
            select 
            src.* 
            from sinduk_ref_codes src
            left join sinduk_ref_code_group srcgr
            on
            src.code_group_id=srcgr.id
            where srcgr.group='Kewarganegaraan'
            )""")

# class SindukRefCodeKlasifikasiPindah(models.Model):
#     _name = 'sinduk.ref.code.klasifikasi.pindah'
#     _inherit = 'sinduk.ref.codes'
#     _rec_name = 'name'
#     _description = 'Klasifikasi Pindah'
#     _order = 'name ASC'
#     _table = 'sinduk_ref_codes'

#     name = fields.Char(
#         string='Name',
#         required=True,
#         # default=lambda self: _('New'),
#         copy=False
#     )


class SindukRefCodeKlasifikasiPindah(models.Model):
    _name = 'sinduk.ref.code.klasifikasi.pindah'
    _auto = False
    name = fields.Char(
        string='Name',
        required=True,
        # default=lambda self: _('New'),
        copy=False
    )

    code = fields.Text(
        string='Kode', readonly=True
    )

    deskripsi = fields.Text(
        string='Deskripsi', readonly=True
    )

    aktif = fields.Boolean(
        string='Aktif', readonly=True
    )

    code_group_id = fields.Many2one(
        string='Code Group',
        comodel_name='sinduk.ref.code.group',
        ondelete='restrict', readonly=True
    )

    def init(self):
        tools.drop_view_if_exists(
            self._cr, 'sinduk_ref_code_klasifikasi_pindah')
        self._cr.execute(""" CREATE VIEW sinduk_ref_code_klasifikasi_pindah AS (
            select 
            src.* 
            from sinduk_ref_codes src
            left join sinduk_ref_code_group srcgr
            on
            src.code_group_id=srcgr.id
            where srcgr.group='Klasifikasi Pindah'
            )""")

# class SindukRefCodeJenisPindah(models.Model):
#     _name = 'sinduk.ref.code.jenis.pindah'
#     _inherit = 'sinduk.ref.codes'
#     _rec_name = 'name'
#     _description = 'Jenis Pindah'
#     _order = 'name ASC'
#     _table = 'sinduk_ref_codes'

#     name = fields.Char(
#         string='Name',
#         required=True,
#         # default=lambda self: _('New'),
#         copy=False
#     )


class SindukRefCodeJenisPindah(models.Model):
    _name = 'sinduk.ref.code.jenis.pindah'
    _auto = False
    name = fields.Char(
        string='Name',
        required=True,
        # default=lambda self: _('New'),
        copy=False
    )

    code = fields.Text(
        string='Kode', readonly=True
    )

    deskripsi = fields.Text(
        string='Deskripsi', readonly=True
    )

    aktif = fields.Boolean(
        string='Aktif', readonly=True
    )

    code_group_id = fields.Many2one(
        string='Code Group',
        comodel_name='sinduk.ref.code.group',
        ondelete='restrict', readonly=True
    )

    def init(self):
        tools.drop_view_if_exists(self._cr, 'sinduk_ref_code_jenis_pindah')
        self._cr.execute(""" CREATE VIEW sinduk_ref_code_jenis_pindah AS (
            select 
            src.* 
            from sinduk_ref_codes src
            left join sinduk_ref_code_group srcgr
            on
            src.code_group_id=srcgr.id
            where srcgr.group='Jenis Pindah'
            )""")

# class SindukRefCodeHari(models.Model):
#     _name = 'sinduk.ref.code.hari'
#     _inherit = 'sinduk.ref.codes'
#     _rec_name = 'name'
#     _description = 'Hari'
#     _order = 'name ASC'
#     _table = 'sinduk_ref_codes'

#     name = fields.Char(
#         string='Name',
#         required=True,
#         # default=lambda self: _('New'),
#         copy=False
#     )


class SindukRefCodeHari(models.Model):
    _name = 'sinduk.ref.code.hari'
    _auto = False
    name = fields.Char(
        string='Name',
        required=True,
        # default=lambda self: _('New'),
        copy=False
    )

    code = fields.Text(
        string='Kode', readonly=True
    )

    deskripsi = fields.Text(
        string='Deskripsi', readonly=True
    )

    aktif = fields.Boolean(
        string='Aktif', readonly=True
    )

    code_group_id = fields.Many2one(
        string='Code Group',
        comodel_name='sinduk.ref.code.group',
        ondelete='restrict', readonly=True
    )

    def init(self):
        tools.drop_view_if_exists(self._cr, 'sinduk_ref_code_hari')
        self._cr.execute(""" CREATE VIEW sinduk_ref_code_hari AS (
            select 
            src.* 
            from sinduk_ref_codes src
            left join sinduk_ref_code_group srcgr
            on
            src.code_group_id=srcgr.id
            where srcgr.group='Hari'
            )""")


class SindukMsPenduduk(models.Model):
    _name = 'sinduk.ms.penduduk'
    _description = 'Master Data Penduduk'
    # _inherit = "hr.employee"
    _rec_name = 'name'
    _order = 'name ASC'

    name = fields.Char(
        string='Name',
        required=True,
        # default=lambda self: _('New'),
        copy=False
    )
    employee_id = fields.Many2one(
        string='Karyawan',
        comodel_name='hr.employee',
        ondelete='restrict',
    )
    # category_ids = fields.Many2many(
    #     string='category',
    #     comodel_name='hr.employee.category',
    #     relation='hr_employee_category_penduduk_rel',
    #     column1='hr_employee_category_id',
    #     column2='penduduk_id',
    # )

    nik_penduduk = fields.Text(
        string='NIK Penduduk',
    )

    nama_lengkap = fields.Text(
        string='Nama Lengkap',
    )

    tgl_lahir = fields.Date(
        string='Tgl Lahir',
        default=fields.Date.context_today,
    )

    jenis_kelamin_id = fields.Many2one(
        string='Jenis Kelamin',
        comodel_name='sinduk.ref.code.jenis.kelamin',
        ondelete='restrict',
    )
    golongan_darah_id = fields.Many2one(
        string='Golongan Darah',
        comodel_name='sinduk.ref.code.golongan.darah',
        ondelete='restrict',
    )
    agama_id = fields.Many2one(
        string='Agama',
        comodel_name='sinduk.ref.code.agama',
        ondelete='restrict',
    )
    pendidikan_terakhir_id = fields.Many2one(
        string='Pendidikan Terakhir',
        comodel_name='sinduk.ref.code.pendidikan.terakhir',
        ondelete='restrict',
    )

    alamat = fields.Text(
        string='Alamat',
    )
    pekerjaan = fields.Text(
        string='Pekerjaan',
    )

    kawin_id = fields.Many2one(
        string='Status Kawin',
        comodel_name='sinduk.ref.code.status.kawin',
        ondelete='restrict',
    )
    no_telp = fields.Text(
        string='Pekerjaan',
    )
    kewarganegaraan_id = fields.Many2one(
        string='Kewarganegaraan',
        comodel_name='sinduk.ref.code.kewarganegaraan',
        ondelete='restrict',
    )

    status_penduduk_id = fields.Many2one(
        string='Status Penduduk',
        comodel_name='sinduk.ref.code.status.penduduk',
        ondelete='restrict',
    )


class SindukTrFormKartuKeluarga(models.Model):
    _name = 'sinduk.tr.form.kartu.keluarga'
    _description = 'Sinduk Form Kartu Keluarga'

    _rec_name = 'name'
    _order = 'name ASC'

    name = fields.Char(
        string='Name',
        required=True,
        # default=lambda self: _('New'),
        copy=False
    )

    no_spkk = fields.Text(
        string='No SPKK',
    )

    tgl_spkk = fields.Date(
        string='Tgl SPKK',
        default=fields.Date.context_today,
    )

    petugas_id = fields.Many2one(
        string='Petugas',
        comodel_name='sinduk.ms.petugas',
        ondelete='restrict',
    )

    kartu_keluarga_detail_ids = fields.One2many(
        string='Kartu Keluarga Detail',
        comodel_name='sinduk.tr.form.kartu.keluarga.detail',
        inverse_name='id',
    )


class SindukTrFormKartuKeluargaDetail(models.Model):
    _name = 'sinduk.tr.form.kartu.keluarga.detail'
    _description = 'Sinduk Form Kartu Keluarga Detail'

    _rec_name = 'name'
    _order = 'name ASC'

    name = fields.Char(
        string='Name',
        required=True,
        # default=lambda self: _('New'),
        copy=False
    )

    kartu_keluarga_id = fields.Many2one(
        string='Kartu Keluarga',
        comodel_name='sinduk.tr.form.kartu.keluarga',
        ondelete='restrict',
    )

    penduduk_id = fields.Many2one(
        string='penduduk',
        comodel_name='sinduk.ms.penduduk',
        ondelete='restrict',
    )


class SindukMsKartuKeluarga(models.Model):
    _name = 'sinduk.ms.kartu.keluarga'
    _description = 'Kartu Keluarga'

    _rec_name = 'name'
    _order = 'name ASC'

    name = fields.Char(
        string='Name',
        required=True,
        # default=lambda self: _('New'),
        copy=False
    )

    no_kk = fields.Text(
        string='No Kk',
    )

    tgl_kk = fields.Date(
        string='Tgl Kk',
        default=fields.Date.context_today,
    )

    form_kartu_keluarga_id = fields.Many2one(
        string='Form Kartu Keluarga',
        comodel_name='sinduk.tr.form.kartu.keluarga',
        ondelete='restrict',
    )


class SindukMsKota(models.Model):
    _name = 'sinduk.ms.kota'
    _description = 'Sinduk Kota'

    _rec_name = 'name'
    _order = 'name ASC'

    name = fields.Char(
        string='Name',
        required=True,
        # default=lambda self: _('New'),
        copy=False
    )

    kode_kota = fields.Text(
        string='Kode Kota',
    )

    provinsi_id = fields.Many2one(
        string='provinsi',
        comodel_name='sinduk.ref.code.provinsi',
        ondelete='restrict',
    )


class SindukTrFormKedatangan(models.Model):
    _name = 'sinduk.tr.form.kedatangan'
    _description = 'Sinduk Form Kedatangan'

    _rec_name = 'name'
    _order = 'name ASC'

    name = fields.Char(
        string='Name',
        required=True,
        # default=lambda self: _('New'),
        copy=False
    )

    no_form_kedatangan = fields.Text(
        string='No Form Kedatangan',
    )

    tgl_form_kedatangan = fields.Date(
        string='Tgl Form Kedatangan',
        default=fields.Date.context_today,
    )

    alasan_pindah = fields.Text(
        string='Alasan Pindah',
    )

    jenis_pindah_id = fields.Many2one(
        string='Jenis Pindah',
        comodel_name='sinduk.ref.code.jenis.pindah',
        ondelete='restrict',
    )

    klasifikasi_pindah_id = fields.Many2one(
        string='Klasifikasi Pindah',
        comodel_name='sinduk.ref.code.klasifikasi.pindah',
        ondelete='restrict',
    )

    kota_id = fields.Many2one(
        string='Kota',
        comodel_name='sinduk.ms.kota',
        ondelete='restrict',
    )

    petugas_id = fields.Many2one(
        string='Petugas',
        comodel_name='sinduk.ms.petugas',
        ondelete='restrict',
    )


class SindukTrFormKelahiran(models.Model):
    _name = 'sinduk.tr.form.kelahiran'
    _description = 'Sinduk Form Kelahiran'

    _rec_name = 'name'
    _order = 'name ASC'

    name = fields.Char(
        string='Name',
        required=True,
        # default=lambda self: _('New'),
        copy=False
    )

    no_form_kelahiran = fields.Text(
        string='No Form Kelahiran',
    )

    tgl_form_kelahiran = fields.Date(
        string='Tgl Form Kelahiran',
        default=fields.Date.context_today,
    )

    nama_kelahiran = fields.Text(
        string='Nama Kelahiran',
    )

    tempat_bersalin = fields.Text(
        string='Tempat Bersalin',
    )

    tempat_dilahirkan = fields.Text(
        string='Tempat Dilahirkan',
    )

    tanggal_waktu_kelahiran = fields.Datetime(
        string='Tanggal Waktu Kelahiran',
        default=fields.Datetime.now,
    )

    jenis_kelamin_id = fields.Many2one(
        string='Jenis Kelamin',
        comodel_name='sinduk.ref.code.jenis.kelamin',
        ondelete='restrict',
    )

    kelahiran_ke = fields.Integer(
        string='Kelahiran Ke',
    )

    nama_penolong_kelahiran = fields.Text(
        string='Nama Penolong Kelahiran',
    )

    berat_kelahiran = fields.Float(
        string='Berat Kelahiran',
    )

    panjang_kelahiran = fields.Float(
        string='Panjang Kelahiran',
    )

    tgl_catatan_kawin = fields.Date(
        string='Tgl Catatan Kawin',
        default=fields.Date.context_today,
    )
    nama_pelapor = fields.Text(
        string='Nama Pelapor',
    )

    nik_pelapor = fields.Text(
        string='NIK Pelapor',
    )

    pelapor_id = fields.Many2one(
        string='Pelapor',
        comodel_name='sinduk.ms.penduduk',
        ondelete='restrict',
    )
    nama_saksi1 = fields.Text(
        string='Nama Saksi 1',
    )

    nik_saksi1 = fields.Text(
        string='NIK Saksi 1',
    )

    saksi1_id = fields.Many2one(
        string='Saksi 1',
        comodel_name='sinduk.ms.penduduk',
        ondelete='restrict',
    )

    nama_saksi2 = fields.Text(
        string='Nama Saksi 2',
    )

    nik_saksi2 = fields.Text(
        string='NIK Saksi2',
    )
    saksi2_id = fields.Many2one(
        string='Saksi 2',
        comodel_name='sinduk.ms.penduduk',
        ondelete='restrict',
    )

    nama_ayah = fields.Text(
        string='Nama Ayah',
    )

    nik_ayah = fields.Text(
        string='NIK Ayah',
    )

    ayah_id = fields.Many2one(
        string='Ayah',
        comodel_name='sinduk.ms.penduduk',
        ondelete='restrict',
    )

    nama_ibu = fields.Text(
        string='Nama Ibu',
    )

    nik_ibu = fields.Text(
        string='NIK Ibu',
    )

    ibu_id = fields.Many2one(
        string='Ibu',
        comodel_name='sinduk.ms.penduduk',
        ondelete='restrict',
    )

    petugas_id = fields.Many2one(
        string='Petugas',
        comodel_name='sinduk.ms.petugas',
        ondelete='restrict',
    )


class SindukTrFormMeninggal(models.Model):
    _name = 'sinduk.tr.form.meninggal'
    _description = 'Sinduk Form Meninggal'

    _rec_name = 'name'
    _order = 'name ASC'

    name = fields.Char(
        string='Name',
        required=True,
        # default=lambda self: _('New'),
        copy=False
    )

    no_form_meninggal = fields.Text(
        string='No Form Meninggal',
    )

    tgl_form_meninggal = fields.Date(
        string='Tgl Form Meninggal',
        default=fields.Date.context_today,
    )

    waktu_meninggal = fields.Datetime(
        string='Waktu Meninggal',
        default=fields.Datetime.now,
    )

    sebab_kematian = fields.Text(
        string='Sebab Kematian',
    )

    jenis_sebab_kematian = fields.Text(
        string='Jenis Sebab Kematian',
    )

    tempat_kematian = fields.Text(
        string='Tempat Kematian',
    )

    penduduk_id = fields.Many2one(
        string='penduduk',
        comodel_name='sinduk.ms.penduduk',
        ondelete='restrict',
    )
    umur_kematian = fields.Integer(
        string='Umur Kematian',
    )
    kartu_keluarga_id = fields.Many2one(
        string='Kartu Keluarga',
        comodel_name='sinduk.ms.kartu.keluarga',
        ondelete='restrict',
    )

    yang_menerangkan = fields.Text(
        string='Yang Menerangkan',
    )
    menerangkan_id = fields.Many2one(
        string='Menerangkan',
        comodel_name='sinduk.ms.penduduk',
        ondelete='restrict',
    )

    nama_pelapor = fields.Text(
        string='Nama Pelapor',
    )

    nik_pelapor = fields.Text(
        string='NIK Pelapor',
    )
    pelapor_id = fields.Many2one(
        string='Pelapor',
        comodel_name='sinduk.ms.penduduk',
        ondelete='restrict',
    )
    nama_saksi1 = fields.Text(
        string='Nama Saksi 1',
    )

    nik_saksi1 = fields.Text(
        string='NIK Saksi1',
    )
    saksi1_id = fields.Many2one(
        string='Saksi 1',
        comodel_name='sinduk.ms.penduduk',
        ondelete='restrict',
    )
    nama_saksi2 = fields.Text(
        string='Nama Saksi 2',
    )

    nik_saksi2 = fields.Text(
        string='NIK Saksi2',
    )

    saksi2_id = fields.Many2one(
        string='Saksi 2',
        comodel_name='sinduk.ms.penduduk',
        ondelete='restrict',
    )

    nama_ayah = fields.Text(
        string='Nama Ayah',
    )

    nik_ayah = fields.Text(
        string='NIK Ayah',
    )
    ayah_id = fields.Many2one(
        string='Ayah',
        comodel_name='sinduk.ms.penduduk',
        ondelete='restrict',
    )

    nama_ibu = fields.Text(
        string='Nama Ibu',
    )

    nik_ibu = fields.Text(
        string='NIK Ibu',
    )
    ibu_id = fields.Many2one(
        string='Ibu',
        comodel_name='sinduk.ms.penduduk',
        ondelete='restrict',
    )
    petugas_id = fields.Many2one(
        string='Petugas',
        comodel_name='sinduk.ms.petugas',
        ondelete='restrict',
    )
