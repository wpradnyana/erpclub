# -*- coding: utf-8 -*-
{
    'name': "sinduk",

    'summary': """
        Aplikasi Catatan Kependudukan 2020""",

    'description': """
        Aplikasi Catatan Kependudukan 2020, berisikan fitur-fitur lengkap.
    """,

    'author': "I Wayan Widi Pradnyana",
    'website': "https://fik.upnvj.ac.id",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/13.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','hr'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
        'reports/report.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
